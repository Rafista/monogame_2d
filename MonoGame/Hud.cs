﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Hud
    {
        private Icon icon_weapon1, icon_weapon2, icon_weapon3;
        private bool weapon1, weapon2, weapon3;
        private int selectedWeapon;

        public Hud(int weapon)
        {
            this.selectedWeapon = weapon;
            if (selectedWeapon == 0)
            {
                weapon1 = true;
                weapon2 = false;
                weapon3 = false;
            }
            else if (selectedWeapon == 1)
            {
                weapon1 = false;
                weapon2 = true;
                weapon3 = false;
            }
            else if (selectedWeapon == 2)
            {
                weapon1 = false;
                weapon2 = false;
                weapon3 = true;
            }

            icon_weapon1 = new Icon(IconType.swordWooden, Anchor.BottomCenter, 1, weapon1);
            icon_weapon2 = new Icon(IconType.pickaxeWooden, Anchor.AutoInline, 1, weapon2);
            icon_weapon3 = new Icon(IconType.axeWooden, Anchor.AutoInline, 1, weapon3);

            UserInterface.Active.AddEntity(icon_weapon1);
            UserInterface.Active.AddEntity(icon_weapon2);
            UserInterface.Active.AddEntity(icon_weapon3); 
        }

        public void HideElement()
        {
            icon_weapon1.Visible = false;
            icon_weapon2.Visible = false;
            icon_weapon3.Visible = false;
        }

    }
}
