﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class GameObject
    {
        //FIELDS
        public Vector2 velocity;
        protected Texture2D texture;
        protected Sprite head;
        protected Sprite body;
        protected Sprite armLeft;
        protected Sprite armRight;
        public Rectangle destRectangle;
        private int timer;
        private int id;
        private bool player;

        private Scale scale = new Scale();

        //GETTER
        public Rectangle Rectangle
        {
            get
            {
                return destRectangle;
            }
        }

        //CONSTRUCTOR
        public GameObject(Player player, WeaponType type) //weapon
        {
            switch (type)
            {
                case WeaponType.SWORD:
                    this.texture = Resources.Images["swordWooden"];
                    break;
                case WeaponType.AXE:
                    this.texture = Resources.Images["axeWooden"];
                    break;
                case WeaponType.PICKAXE:
                    this.texture = Resources.Images["pickaxeWooden"];
                    break;
            }
        }

        public GameObject(Enemy enemy, WeaponType type) //enemy weapon
        {
            this.player = false;
            switch (type)
            {
                case WeaponType.SKELETONSWORD:
                    this.texture = Resources.Images["swordSkeleton"];
                    break;
            }
        }

        public GameObject(int x, int y) //character
        {
            this.player = true;
            this.texture = Resources.Images["player"];
            this.destRectangle = new Rectangle(x - ((int)(this.texture.Width * scale.GetScale()) / 2), y, (int)(this.texture.Width * scale.GetScale()), (int)(this.texture.Height * scale.GetScale()));
            this.id = 1;
        }

        public GameObject(int x, int y, EnemyType type) // ENEMY
        {
            this.player = false;
            switch (type)
            {
                case EnemyType.PIG:
                    this.texture = Resources.Images["pig"];
                    break;
                case EnemyType.SKELETON:
                    this.texture = Resources.Images["skeleton"];
                    break;
            }

            this.destRectangle = new Rectangle(x - ((int)(this.texture.Width * scale.GetScale()) / 2), y, (int)(this.texture.Width * scale.GetScale()), (int)(this.texture.Height * scale.GetScale()));

        }

        //METHODS
        public void animatePlayer(GameTime gameTime)
        {
            if (player)
            {
                timer += gameTime.ElapsedGameTime.Milliseconds;
                if (timer >= 100)
                {
                    timer = 0;
                    this.texture = Resources.Images["player" + id];
                    if (id >= 5)
                        id = 1;
                    else
                        id++;
                }
            }
        }

        public void animatePlayer(int x)
        {
            if (player)
                this.texture = Resources.Images["player"];
        }

        public bool IsTouchingLeft(Sprite sprite)
        {
            return this.Rectangle.Right + (1f * scale.GetScale()) > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Left &&
                this.Rectangle.Bottom > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Bottom;
        }

        public bool IsTouchingRight(Sprite sprite)
        {
            return this.Rectangle.Left - (1f * scale.GetScale()) < sprite.Rectangle.Right &&
                this.Rectangle.Right > sprite.Rectangle.Right &&
                this.Rectangle.Bottom > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Bottom;
        }

        public bool IsTouchingTop(Sprite sprite)
        {
            return this.Rectangle.Bottom + (4f * scale.GetScale()) > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Top &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Right;
        }

        public bool IsTouchingBottom(Sprite sprite)
        {
            return this.Rectangle.Top + this.velocity.Y < sprite.Rectangle.Bottom &&
                this.Rectangle.Bottom > sprite.Rectangle.Bottom &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Right;
        }

        //UPDATE & DRAW
        public virtual void Update(GameTime gameTime, Input input, Game1 game)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
