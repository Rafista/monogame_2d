﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    class LoadingScreen : MenuBase
    {
        private Rectangle background;
        private Texture2D logo;
        private Video video;
        private VideoPlayer player;
        private Texture2D videoTexture;


        private Game1 game;
        private Scale scale = new Scale();
        private SpriteFont spriteFont = GeonBit.UI.Resources.Fonts[0];


        private string loadingScreenName;
        private string stringDot = "";
        private string textToShow = "";
        private int dotCounter = 0;
        private bool canSkip = false;

        public LoadingScreen(string loadingScreenName) : base()
        {
            //this.video = Resources.Videos["movie"];
            //this.player = new VideoPlayer();
            this.videoTexture = null;
            this.loadingScreenName = loadingScreenName;
            this.background = new Rectangle(0, 0, Settings.ScreenWidth, Settings.ScreenHeight);
            this.logo = Resources.Images["icon"];
            dotCounter = 0;
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            //player.Play(video);

            KeyboardState keyState = Keyboard.GetState();
            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected || !Settings.gamepad)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick || !Settings.gamepad)
                {
                    if ((keyState.IsKeyDown(Keys.Space) || state.IsButtonDown(Buttons.A) || state.IsButtonDown(Buttons.B) 
                        || state.IsButtonDown(Buttons.X) || state.IsButtonDown(Buttons.Y)) && canSkip)
                    {
                        game.ChangeScene(Scene.MENU, "");
                    }
                }
            }

            if (gameTime.TotalGameTime.Seconds > 1)
            {
                canSkip = true;
                dotCounter++;
                if (dotCounter % 30 == 0)
                {
                    stringDot += ".";
                    dotCounter = 0;
                }
                if (stringDot.Length == 4)
                    stringDot = "";

                textToShow = Text.press_space_to_continue + stringDot;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            //videoTexture = player.GetTexture();

            //if (videoTexture != null)
            //    spriteBatch.Draw(videoTexture, new Rectangle(0, 0, 400, 240), Color.White);
            // Console.WriteLine((Settings.ScreenWidth / (float)logo.Width));
            spriteBatch.Draw(this.logo, new Vector2(0, 0), scale: new Vector2(Settings.ScreenWidth / (float)logo.Width, Settings.ScreenHeight / (float)logo.Height));
            spriteBatch.DrawString(spriteFont, textToShow, new Vector2((float)Settings.ScreenWidth / 2 - (float)(scale.TextWidth(Text.press_space_to_continue, 0) / 2), Settings.ScreenHeight - Settings.ScreenHeight / 15), Color.Black);

        }
    }
}
