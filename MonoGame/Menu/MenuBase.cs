﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class MenuBase
    {
        //UPDATE & DRAW
        public virtual void Update(GameTime gameTime, Input input, Game1 game)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
