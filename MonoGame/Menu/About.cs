﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace MonoGame
{
    class About : MenuBase
    {
        private Scale scaleElement = new Scale();
        private Panel panel;
        private Button button_back;

        private MulticolorParagraph colorParagraph;

        public About() : base()
        {
            // create a panel and position in center of screen
            float panelWidth = (float)(Settings.ScreenWidth * 0.65);
            float panelHeight = (float)(Settings.ScreenHeight * 0.8);

            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;

            panel = new Panel(new Vector2((float)panelWidth / scaleValue, (float)panelHeight / scaleValue), PanelSkin.Default); //Anchor.Center
            UserInterface.Active.AddEntity(panel);

            panel.AddChild(new Header(Text.about));
            panel.AddChild(new HorizontalLine());

            colorParagraph = new MulticolorParagraph(@"Authors of {{RED}}GAME TITLE {{BLUE}}v. 0.1.0:
{{DEFAULT}}-Robert 'ReZuS' 'Robcioman' Buritta,
-Rafał 'Rafista' Stanek.

{{YELLOW}}Changelog:
{{DEFAULT}}-dodano nową postać Arizus!
");
           
            panel.AddChild(colorParagraph);
           
            button_back = new Button(Text.back, ButtonSkin.Default, Anchor.BottomCenter,size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, 0 / scaleElement.GetScaleY()));
            panel.AddChild(button_back);            
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            base.Update(gameTime, input, game);

            button_back.OnClick = (Entity btn) =>
            {
                panel.Visible = false;
                game.ChangeScene(Scene.MENU, "");
            };
        }
    }
}
