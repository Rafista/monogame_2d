﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace MonoGame
{
    public class Options : MenuBase
    {
        //FIELDS
        private ReadXml readXml = new ReadXml();

        private bool changed = false;

        private Button button_save_options;
        private Button button_back;
        private Button button_default;

        private DropDown list_languages;
        private DropDown list_resolutions;

        private CheckBox checkBox_full_screen;

        private Slider slide_music;
        private Slider slide_master;
        private Slider slide_effect;

        private Panel panel;
        private PanelTabs tabs = new PanelTabs();

        private Dictionary<string, string> langaugeList;

        private PlaySound playSound;

        private Paragraph paragraph_music;
        private Paragraph paragraph_master;
        private Paragraph paragraph_effect;

        private Scale scaleElement = new Scale();
        private LoadSettings loadSettings = new LoadSettings();
        private Game1 game;

        //CONSTRUCTOR
        public Options() : base()
        {
            langaugeList = readXml.GetLanguageFileList();

            // create a panel and position in center of screen
            float panelWidth = (float)(Settings.ScreenWidth * 0.9);
            float panelHeight = (float)(Settings.ScreenHeight * 0.9);

            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;

            panel = new Panel(new Vector2((float)panelWidth / scaleValue, (float)panelHeight / scaleValue), PanelSkin.Default); //Anchor.Center
            UserInterface.Active.AddEntity(panel);

            panel.AddChild(tabs);

            // panel.AddChild(new Header(Text.options));
            //panel.AddChild(new HorizontalLine());

            {
                PanelTabs.TabData tab_video = tabs.AddTab(Text.video);
                tab_video.panel.AddChild(new Paragraph("\n" + Text.resolution));

                //filling resolution to list
                list_resolutions = new DropDown(new Vector2(300, 200));
                list_resolutions.DefaultText = Settings.ScreenWidth + "x" + Settings.ScreenHeight;
                foreach (string element in scaleElement.GetAvailableResolution())
                {
                    list_resolutions.AddItem(element);
                }

                tab_video.panel.AddChild(list_resolutions);

                //full screen checkbox
                checkBox_full_screen = new CheckBox(Text.full_screen, size: new Vector2(scaleElement.TextWidth(Text.full_screen, 0) / scaleValue, scaleElement.TextHeight(Text.full_screen, 0) / scaleValue));
                if (Settings.Fullscreen)
                    checkBox_full_screen.Checked = true;
                else
                    checkBox_full_screen.Checked = false;
                tab_video.panel.AddChild(checkBox_full_screen);

            }

            {
                PanelTabs.TabData tab_audio = tabs.AddTab(Text.audio);

                //Gloscnosc ogolna
                paragraph_master = new Paragraph("\n" + Text.sound_master + ": " + (float)Settings.masterVolume);
                tab_audio.panel.AddChild(paragraph_master);
                slide_master = new Slider(0, 100, size: new Vector2(220, 50));
                slide_master.Value = (int)(Settings.masterVolume * 100);
                tab_audio.panel.AddChild(slide_master);

                //Glosnosc muzyki
                paragraph_music = new Paragraph("\n" + Text.sound_music + ": " + (float)Settings.musicVolume);
                tab_audio.panel.AddChild(paragraph_music);
                slide_music = new Slider(0, 100, size: new Vector2(220, 50));
                slide_music.Value = (int)(Settings.musicVolume * 100);
                tab_audio.panel.AddChild(slide_music);

                //Glosnosc efektow
                paragraph_effect = new Paragraph("\n" + Text.sound_effect + ": " + (float)Settings.effectVolume);
                tab_audio.panel.AddChild(paragraph_effect);
                slide_effect = new Slider(0, 100, size: new Vector2(220, 50));
                slide_effect.Value = (int)(Settings.effectVolume * 100);
                tab_audio.panel.AddChild(slide_effect);
            }

            {
                PanelTabs.TabData tab_control = tabs.AddTab(Text.control);

            }

            //panel.PanelOverflowBehavior = PanelOverflowBehavior.VerticalScroll;
            //panel.Scrollbar.AdjustMaxAutomatically = true;
            //panel.Identifier = "panel_with_scrollbar";
            {
                PanelTabs.TabData tab_other = tabs.AddTab(Text.other);

                tab_other.panel.AddChild(new Paragraph(Text.language_select, Anchor.TopLeft, offset: new Vector2(0, 50 / scaleElement.GetScaleY())));

                //filling languages to list
                list_languages = new DropDown(new Vector2(200, 200), Anchor.TopLeft, offset: new Vector2(0, 80 / scaleElement.GetScaleY()));

                foreach (KeyValuePair<string, string> element in langaugeList)
                {
                    list_languages.AddItem(element.Value);
                }

                list_languages.DefaultText = GetValuePerKeyList(Settings.language);
                tab_other.panel.AddChild(list_languages);
            }

            button_save_options = new Button(Text.save, ButtonSkin.Default, Anchor.BottomCenter, size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, 100 / scaleElement.GetScaleY()));
            panel.AddChild(button_save_options);

            button_default = new Button(Text.default_value, ButtonSkin.Default, Anchor.BottomCenter, size: new Vector2(scaleElement.TextWidth(Text.default_value, 0) / scaleValue, scaleElement.TextHeight(Text.default_value, 0) / scaleValue), offset: new Vector2(0, 50 / scaleElement.GetScaleY()));
            panel.AddChild(button_default);

            button_back = new Button(Text.back, ButtonSkin.Default, Anchor.BottomCenter, size: new Vector2(scaleElement.TextWidth(Text.back, 0) / scaleValue, scaleElement.TextHeight(Text.back, 0) / scaleValue), offset: new Vector2(0, 0 / scaleElement.GetScaleY()));
            panel.AddChild(button_back);

            button_default.OnClick += (GeonBit.UI.Entities.Entity entity) =>
            {
                GeonBit.UI.Utils.MessageBoxUI.ShowMsgBox(Text.alert, Text.default_alert, new GeonBit.UI.Utils.MessageBoxUI.MsgBoxOption[] {
                                new GeonBit.UI.Utils.MessageBoxUI.MsgBoxOption(Text.yes, () => {
                                     loadSettings.Load(Settings.StartedScreenWidth,Settings.StartedScreenHeight);
                                     loadSettings.SetDefaultSettings();
                                     readXml.ReloadXmlFile();
                                     readXml.SaveXmlContent();
                                     panel.Visible = false;
                                     game.SetVideoSettings();
                                     game.ChangeScene(Scene.OPTIONS,"");
                                     game.ChangeMusicVolume();
                                     loadSettings.SaveToFile();
                                    return true;
                                }),
                                new GeonBit.UI.Utils.MessageBoxUI.MsgBoxOption(Text.no, () => { return true; })
                                });
            };



        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            base.Update(gameTime, input, game);
            this.game = game;
            tabs.OnValueChange = (Entity entity) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
            };

            button_back.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                panel.Visible = false;
                game.ChangeScene(Scene.MENU,"");
            };


            button_save_options.OnClick = (Entity btn) =>
            {
                Settings.musicVolume = slide_music.Value / 100.0;
                Settings.masterVolume = slide_master.Value / 100.0;
                Settings.effectVolume = slide_effect.Value / 100.0;
                if (changed)
                {
                    Settings.ScreenWidth = Int32.Parse(list_resolutions.SelectedValue.Split('x')[0]);
                    Settings.ScreenHeight = Int32.Parse(list_resolutions.SelectedValue.Split('x')[1]);
                    changed = false;
                }
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                readXml.ReloadXmlFile();
                readXml.SaveXmlContent();
                panel.Visible = false;
                game.SetVideoSettings();
                game.ChangeScene(Scene.OPTIONS,"");
                game.ChangeMusicVolume();
                loadSettings.SaveToFile();
            };


            list_languages.OnValueChange = (Entity entity) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                Settings.language = GetKeyPerValueList(list_languages.SelectedValue);
            };

            list_resolutions.OnValueChange = (Entity entity) =>
            {
                changed = true;
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
            };

            checkBox_full_screen.OnValueChange = (Entity checkbox) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                Settings.Fullscreen = !Settings.Fullscreen;
            };

            slide_music.OnValueChange = (Entity entity) =>
            {
                Settings.musicVolume = (float)(slide_music.Value / 100.00);
                paragraph_music.Text = ("\n" + Text.sound_music + ": " + (float)Settings.musicVolume);
                game.ChangeMusicVolume();
            };

            slide_master.OnValueChange = (Entity entity) =>
            {
                Settings.masterVolume = (float)(slide_master.Value / 100.00);
                paragraph_master.Text = ("\n" + Text.sound_master + ": " + (float)Settings.masterVolume);
                game.ChangeMusicVolume();
            };

            slide_effect.OnValueChange = (Entity entity) =>
            {
                Settings.effectVolume = (float)(slide_effect.Value / 100.00);
                paragraph_effect.Text = ("\n" + Text.sound_effect + ": " + (float)Settings.effectVolume);
                game.ChangeMusicVolume();
            };
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        private string GetValuePerKeyList(string key)
        {
            string value = "";
            foreach (KeyValuePair<string, string> element in langaugeList)
            {
                if (element.Key.Equals(key))
                {
                    value = element.Value;
                    break;
                }
            }
            return value;
        }

        private string GetKeyPerValueList(string value)
        {
            string key = "";
            foreach (KeyValuePair<string, string> element in langaugeList)
            {
                if (element.Value.Equals(value))
                {
                    key = element.Key;
                    break;
                }
            }
            return key;
        }
    }
}
