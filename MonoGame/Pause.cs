﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MonoGame
{
    public class Pause : MenuBase
    {
        private Scale scaleElement = new Scale();

        private PlaySound playSound;
        private Panel panel;
        private Button button_resume, button_end_game, button_exit_game;
        private Game1 game;

        public Pause(Game1 game, Core core, Hud hud) : base()
        {
            this.game = game;
            // create a panel and position in center of screen
            float panelWidth = (float)(Settings.ScreenWidth * 0.35);
            float panelHeight = (float)(Settings.ScreenHeight * 0.3);

            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;

            panel = new Panel(new Vector2((float)panelWidth / scaleValue, (float)panelHeight / scaleValue), PanelSkin.Default); //Anchor.Center
            UserInterface.Active.AddEntity(panel);

            button_resume = new Button(Text.resume, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.resume, 0) / scaleValue, scaleElement.TextHeight(Text.resume, 0) / scaleValue), offset: new Vector2(0,10*scaleElement.GetScaleY()));
            panel.AddChild(button_resume);

            button_end_game = new Button(Text.back_to_menu, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.back_to_menu, 0) / scaleValue, scaleElement.TextHeight(Text.back_to_menu, 0) / scaleValue));
            panel.AddChild(button_end_game);

            button_exit_game = new Button(Text.exit_game, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.exit_game, 0) / scaleValue, scaleElement.TextHeight(Text.exit_game, 0) / scaleValue));
            panel.AddChild(button_exit_game);

            button_resume.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                panel.Visible = false;
                core.SetIsVisible(false);
            };

            button_exit_game.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.Exit();
            };

            button_end_game.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                panel.Visible = false;
                hud.HideElement();
                game.ChangeScene(Scene.MENU, "");
            };

        }

        public void SetPanelVisible(bool isVisible)
        {
            panel.Visible = isVisible;
        }



    }
}