﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Settings
    {
        public static int ScreenWidth;
        public static int ScreenHeight;
        public static bool Fullscreen;

        public static string language;

        public static double masterVolume;
        public static double musicVolume;
        public static double effectVolume;

        public static string pathLanguage = @"Languages/"; 

        public static float PixelRatio = 1;

        public static int StartedScreenWidth;
        public static int StartedScreenHeight;

        public static bool gamepad = false;
    }
}
