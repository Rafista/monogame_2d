﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Resources
    {
        public static Dictionary<string, Texture2D> Images;
        public static Dictionary<string, SoundEffect> Sounds;
        public static Dictionary<string, Song> Songs;
        public static Dictionary<string, Video> Videos;

        public static void LoadImages(ContentManager content)
        {
            Images = new Dictionary<string, Texture2D>();
            List<string> graphics = new List<string>()
            {
                "background",
                "logo",
                "icon",
                "hitbox",
                //map items
                "dirt",
                "blank",
                "air",
                "air1",
                "air2",
                "air3",
                "grass",
                "tree",
                "door",
                //player
                "player",
                "player1",
                "player2",
                "player3",
                "player4",
                "player5",
                //monsters
                "skeleton",
                "pig",
                //tools
                "swordWooden",
                "axeWooden",
                "pickaxeWooden",
                "swordSkeleton",
                //chests
                "chest",
                "chestgold",
                //materials
                "wood",
                "stone",
                "stoneOre",
                "iron",
                "ironOre",
                "gold",
                "goldOre",
                "diamond",
                "diamondOre",
                //monster items
                "bone",
                "hide",
                "meat"
            };

            foreach (string img in graphics)
                Images.Add(img, content.Load<Texture2D>("Graphics/" + img));
        }

        public static void LoadSounds(ContentManager content)
        {
            Sounds = new Dictionary<string, SoundEffect>();

            List<string> sounds = new List<string>()
            {
                "click",
                "jump",
                "axe",
                "pickaxe",
                "sword",
                "pig",
                "switch"
            };

            foreach (string sfx in sounds)
                Sounds.Add(sfx, content.Load<SoundEffect>("Sounds/" + sfx));
        }

        public static void LoadSongs(ContentManager content)
        {
            Songs = new Dictionary<string, Song>();

            List<string> songs = new List<string>()
            {
                "bgmusic"
            };

            foreach (string sfx in songs)
                Songs.Add(sfx, content.Load<Song>("Songs/" + sfx));
        }

        public static void LoadVideos(ContentManager content)
        {
            Videos = new Dictionary<string, Video>();

            List<string> videos = new List<string>()
            {
                "movie"
            };

            foreach (string sfx in videos)
                Videos.Add(sfx, content.Load<Video>("Videos/" + sfx));
        }

    }
}
