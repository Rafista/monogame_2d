﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGame
{
    public class Core : MenuBase
    {
        //FIELDS
        public Player player;
        private List<Enemy> enemies = new List<Enemy>();
        private List<Notification> notifications = new List<Notification>();
        private List<Weapon> enemyWeapons;
        private List<Weapon> weapons;
        public List<Sprite> map;
        private List<Sprite> clouds;
        private Camera camera;
        private int timerChest;
        private int timerPickUp;
        private int timerHit;
        private Random random = new Random();
        private int count;
        private List<Sprite> drop;
        public Equipment equipment;
        private bool isPause;
        private long timerPause = 0;

        public long timeInGame = 0L;
        public long timeInPause = 0L;
        public int goldchestOpened = 0;
        public int chestOpened = 0;
        private bool isEndGame = false;

        private bool isChestOpening = false;
        private bool isGoldChestOpening = false;

        private double frameRate;
        private double frameRateMin = 99999.0;
        private double frameRateMax = 0;

        private const int TOTAL_PIG = 15;
        private List<Vector2> pigPosition = new List<Vector2>();

        private Pause pause;
        private Statistics statistics;
        private Scale scale = new Scale();
        private Hud hud;
        private SpriteFont spriteFont = GeonBit.UI.Resources.Fonts[0];
        private Map mapClass;
        private PlaySound weaponSound;

        //CONSTRUCTOR
        public Core() : base()
        {
            mapClass = new Map();
            this.map = mapClass.getList();
            this.clouds = mapClass.getClouds();
            this.hud = new Hud(0);
            this.player = new Player((int)GetSpawnPosition().X, (int)GetSpawnPosition().Y, this);

            AddPigOnMap();

            this.weapons = new List<Weapon>() {
                new Weapon(this.player, WeaponType.SWORD),
                new Weapon(this.player, WeaponType.PICKAXE),
                new Weapon(this.player, WeaponType.AXE),
            };

            this.enemyWeapons = new List<Weapon>()
            {
                //new Weapon(this.enemies[1], WeaponType.SKELETONSWORD)
            };

            this.camera = new Camera();
            this.count = 0;
            this.drop = new List<Sprite>();
            this.equipment = new Equipment();

            frameRateMin = 99999.0;
        }

        //METHODS
        private void DropItem(Sprite sprite)
        {
            switch (sprite.getSpriteType())
            {
                case SpriteType.WOOD:
                    drop.Add(new Sprite(Resources.Images["wood"], sprite));
                    break;
                case SpriteType.STONE:
                    drop.Add(new Sprite(Resources.Images["stoneOre"], sprite));
                    break;
                case SpriteType.IRON:
                    drop.Add(new Sprite(Resources.Images["ironOre"], sprite));
                    break;
                case SpriteType.GOLD:
                    drop.Add(new Sprite(Resources.Images["goldOre"], sprite));
                    break;
                case SpriteType.DIAMOND:
                    drop.Add(new Sprite(Resources.Images["diamondOre"], sprite));
                    break;
                case SpriteType.BONE:
                    drop.Add(new Sprite(Resources.Images["bone"], sprite));
                    break;
                case SpriteType.HIDE:
                    drop.Add(new Sprite(Resources.Images["hide"], sprite));
                    break;
                case SpriteType.CHEST:
                    count = random.Next(1, 4);
                    ChestDrop(count, sprite);
                    break;
                case SpriteType.GOLDCHEST:
                    count = random.Next(1, 6);
                    ChestDrop(count, sprite);
                    break;
                case SpriteType.OTHER:
                    break;
            }
        }

        private void AddPigOnMap()
        {
            int mapCounter = 0;

            int[] tab = new int[TOTAL_PIG];
            for (int i = 0; i < TOTAL_PIG; i++)
            {
                tab[i] = random.Next(1, mapClass.GetCountWalkableElement() + 1);
            }

            foreach (Sprite mapElement in map)
            {
                if (mapElement.getSpriteType() == SpriteType.AIR)
                {
                    for (int i = 0; i < tab.Length; i++)
                    {
                        if (tab[i] == mapCounter)
                        {
                            enemies.Add(new Enemy(mapElement.destRectangle.X, mapElement.destRectangle.Y + 10, EnemyType.PIG, this));
                        }
                    }
                    mapCounter++;
                }

            }

        }

        private void DropLoot(Enemy enemy)
        {
            int count = random.Next(1, 4);

            switch (enemy.getEnemyType())
            {
                case EnemyType.PIG:
                    for (int i = 0; i < count; i++)
                    {
                        int r = random.Next(1, 3);

                        if (r == 1)
                            drop.Add(new Sprite(Resources.Images["meat"], enemy, r));
                        else if (r == 2)
                            drop.Add(new Sprite(Resources.Images["hide"], enemy, r));
                    }
                    break;
                case EnemyType.SKELETON:
                    for (int i = 0; i < count; i++)
                        drop.Add(new Sprite(Resources.Images["bone"], enemy, 0));
                    break;
            }
        }

        private Vector2 GetSpawnPosition()
        {
            int posX = 0, posY = 0;
            foreach (Sprite sprite in map)
            {
                if (sprite.getSpriteType() == SpriteType.SPAWN)
                {
                    posX = sprite.Rectangle.X;
                    posY = sprite.Rectangle.Y;
                    break;
                }
            }
            return new Vector2(posX, posY);
        }

        public void ReloadHud(int weapon)
        {
            this.hud.HideElement();
            this.hud = new Hud(weapon);
        }

        private void ChestDrop(int c, Sprite sprite)
        {
            int loot;
            for (int i = 0; i < c; i++)
            {
                loot = random.Next(0, 7);
                switch (loot)
                {
                    case 0:
                        drop.Add(new Sprite(Resources.Images["wood"], sprite, SpriteType.WOOD));
                        break;
                    case 1:
                        drop.Add(new Sprite(Resources.Images["stoneOre"], sprite, SpriteType.STONE));
                        break;
                    case 2:
                        drop.Add(new Sprite(Resources.Images["ironOre"], sprite, SpriteType.IRON));
                        break;
                    case 3:
                        drop.Add(new Sprite(Resources.Images["goldOre"], sprite, SpriteType.GOLD));
                        break;
                    case 4:
                        drop.Add(new Sprite(Resources.Images["diamondOre"], sprite, SpriteType.DIAMOND));
                        break;
                    case 5:
                        drop.Add(new Sprite(Resources.Images["bone"], sprite, SpriteType.BONE));
                        break;
                    case 6:
                        drop.Add(new Sprite(Resources.Images["hide"], sprite, SpriteType.HIDE));
                        break;
                }
            }
        }

        public void SetIsVisible(bool isPause)
        {
            this.isPause = isPause;
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            base.Update(gameTime, input, game);

            KeyboardState keyState = Keyboard.GetState();
            this.timerPause += gameTime.ElapsedGameTime.Milliseconds;

            //PAUSE
            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected || !Settings.gamepad)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick || !Settings.gamepad)
                {
                    if (timerPause > 150)
                    {
                        if ((keyState.IsKeyDown(Keys.Escape) || state.IsButtonDown(Buttons.Start)) && !player.isInventoryOpen)
                        {
                            if (!this.isPause)
                            {
                                this.isPause = true;
                                pause = new Pause(game, this, hud);
                                timerPause = 0;
                            }
                            else
                            {
                                this.isPause = false;
                                pause.SetPanelVisible(false);
                                timerPause = 0;
                            }
                        }
                    }
                }
            }

            if (!isPause)
            {
                this.mapClass.Update(gameTime);

                this.weapons[player.weaponID].Update(gameTime, input, game);

                frameRate = 1.0 / (double)gameTime.ElapsedGameTime.TotalSeconds;
                if (frameRate > frameRateMax)
                    frameRateMax = frameRate;

                if (frameRate < frameRateMin)
                    frameRateMin = frameRate;

                this.player.Update(gameTime, input, game);
                foreach (Weapon w in enemyWeapons)
                    w.Update(gameTime, input, game);
                player.SetGravity(true);

                if (notifications != null)
                {
                    foreach (Notification not in notifications)
                    {
                        not.Update(gameTime, input, game);
                        if (not.timer < 0)
                        {
                            notifications.Remove(not);
                            break;
                        }
                    }
                }

                this.timeInGame += gameTime.ElapsedGameTime.Milliseconds;

                foreach (Enemy enemy in enemies)
                {
                    enemy.Update(gameTime, input, game);
                    enemy.SetGravity(true);
                    GamePadState state = GamePad.GetState(PlayerIndex.One);
                    if ((input.IsLeftMousePressed() || state.IsButtonDown(Buttons.X)) && weapons[player.weaponID].destRectangle.Intersects(enemy.destRectangle))
                    {
                        timerHit += gameTime.ElapsedGameTime.Milliseconds;
                        if (state.IsButtonDown(Buttons.X))
                        {
                            if (timerHit >= 1100)
                            {
                                timerHit = 0;
                                if (weapons[player.weaponID].getWeaponType() == WeaponType.SWORD)
                                {
                                    enemy.hit(2);
                                    notifications.Add(new Notification("2", enemy.destRectangle.X, enemy.destRectangle.Y, 1000));
                                }
                                else
                                {
                                    enemy.hit(1);
                                    notifications.Add(new Notification("1", enemy.destRectangle.X, enemy.destRectangle.Y, 1000));
                                }
                            }
                        }
                        else if (input.IsLeftMousePressed())
                        {
                            if (timerHit >= 100)
                            {
                                timerHit = 0;
                                if (weapons[player.weaponID].getWeaponType() == WeaponType.SWORD)
                                {
                                    enemy.hit(2);
                                    notifications.Add(new Notification("2", enemy.destRectangle.X, enemy.destRectangle.Y, 1000));
                                }
                                else
                                {
                                    enemy.hit(1);
                                    notifications.Add(new Notification("1", enemy.destRectangle.X, enemy.destRectangle.Y, 1000));
                                }
                            }
                        }

                        if (enemy.getHP() <= 0)
                        {
                            enemies.Remove(enemy);
                            DropLoot(enemy);
                            player.destRectangle.Y -= 3;
                            break;
                        }
                    }
                }

                foreach (Sprite s in map)
                {
                    //collisions
                    if (player.velocity.Y >= 0 && player.IsTouchingTop(s) && !s.isElementWalkable())
                    {
                        player.SetGravity(false);
                    }

                    if (player.velocity.Y < 0 && player.IsTouchingBottom(s) && !s.isElementWalkable())
                    {
                        player.velocity.Y = 0;
                    }

                    if (player.velocity.X > 0 && player.IsTouchingLeft(s) && !s.isElementWalkable())
                    {
                        player.velocity.X = 0;
                    }

                    if (player.velocity.X < 0 && player.IsTouchingRight(s) && !s.isElementWalkable())
                    {
                        player.velocity.X = 0;
                    }

                    if (player.destRectangle.Intersects(s.destRectangle))
                    {
                        if (s.getSpriteType() == SpriteType.OTHER)
                            player.destRectangle.Y--;
                    }

                    foreach (Enemy e in enemies)
                    {
                        if (e.velocity.Y >= 0 && e.IsTouchingTop(s) && !s.isElementWalkable())
                        {
                            e.SetGravity(false);
                        }

                        if (e.velocity.Y < 0 && e.IsTouchingBottom(s) && !s.isElementWalkable())
                        {
                            e.velocity.Y = 0;
                        }

                        if (e.velocity.X > 0 && e.IsTouchingLeft(s) && !s.isElementWalkable())
                        {
                            e.velocity.X = 0;
                        }

                        if (e.velocity.X < 0 && e.IsTouchingRight(s) && !s.isElementWalkable())
                        {
                            e.velocity.X = 0;
                        }

                        if (e.destRectangle.Intersects(s.destRectangle))
                        {
                            if (s.getSpriteType() == SpriteType.OTHER)
                                e.destRectangle.Y--;
                        }
                    }

                    //hit a static element
                    if (c.IsConnected || !Settings.gamepad)
                    {
                        GamePadState state = GamePad.GetState(PlayerIndex.One);
                        if (c.HasLeftXThumbStick || !Settings.gamepad)
                        {
                            if ((input.IsLeftMousePressed() || state.IsButtonDown(Buttons.X)) && weapons[player.weaponID].destRectangle.Intersects(s.Rectangle))
                            {
                                timerHit += gameTime.ElapsedGameTime.Milliseconds;
                                if (weapons[player.weaponID].getWeaponType() == WeaponType.AXE && s.getSpriteType() == SpriteType.WOOD ||
                                    weapons[player.weaponID].getWeaponType() == WeaponType.PICKAXE && s.getSpriteType() != SpriteType.WOOD)
                                {
                                    if (state.IsButtonDown(Buttons.X))
                                    {
                                        if (s.getHP() != 99999 && timerHit >= 1100)
                                        {
                                            timerHit = 0;
                                            s.hit(2);
                                            DropItem(s);
                                            if (weapons[player.weaponID].getWeaponType() == WeaponType.AXE)
                                                this.weaponSound = new PlaySound("axe", (float)(Settings.effectVolume * Settings.masterVolume));
                                            else if (weapons[player.weaponID].getWeaponType() == WeaponType.PICKAXE)
                                                this.weaponSound = new PlaySound("pickaxe", (float)(Settings.effectVolume * Settings.masterVolume));
                                        }
                                    }
                                    else if (input.IsLeftMousePressed())
                                    {
                                        if (s.getHP() != 99999 && timerHit >= 150)
                                        {
                                            timerHit = 0;
                                            s.hit(2);
                                            DropItem(s);
                                            if (weapons[player.weaponID].getWeaponType() == WeaponType.AXE)
                                                this.weaponSound = new PlaySound("axe", (float)(Settings.effectVolume * Settings.masterVolume));
                                            else if (weapons[player.weaponID].getWeaponType() == WeaponType.PICKAXE)
                                                this.weaponSound = new PlaySound("pickaxe", (float)(Settings.effectVolume * Settings.masterVolume));
                                        }
                                    }

                                    if (s.getHP() <= 0)
                                    {
                                        map.Remove(s);
                                        player.destRectangle.Y -= 3;
                                        break;
                                    }
                                }
                            }

                            if ((keyState.IsKeyDown(Keys.E) || state.IsButtonDown(Buttons.A)) && weapons[player.weaponID].destRectangle.Intersects(s.Rectangle))
                            {
                                timerChest += gameTime.ElapsedGameTime.Milliseconds;
                                Console.WriteLine(timerChest);
                                if (s.getSpriteType() == SpriteType.CHEST)
                                {
                                    isChestOpening = true;
                                    if (timerChest >= 3000)
                                    {                                      
                                        timerChest = 0;
                                        DropItem(s);
                                        map.Remove(s);
                                        chestOpened++;
                                        player.destRectangle.Y -= 3;
                                        isChestOpening = false;
                                        break;
                                    }
                                }
                                else if (s.getSpriteType() == SpriteType.GOLDCHEST)
                                {
                                    isGoldChestOpening = true;
                                    if (timerChest >= 5000)
                                    {                                     
                                        timerChest = 0;
                                        DropItem(s);
                                        map.Remove(s);
                                        goldchestOpened++;
                                        player.destRectangle.Y -= 3;
                                        isGoldChestOpening = false;
                                        break;
                                    }
                                }
                                else if (s.getSpriteType() == SpriteType.DOOR)
                                {
                                    if (!this.isEndGame)
                                    {
                                        this.isEndGame = true;
                                        this.isPause = true;
                                        statistics = new Statistics(game, this, hud);
                                    }
                                    else
                                    {
                                        this.isEndGame = false;
                                        this.isPause = false;
                                    }

                                }
                                
                            }


                            if (c.IsConnected)
                            {
                                if (keyState.IsKeyUp(Keys.E) && state.Buttons.A == ButtonState.Released)
                                {
                                    timerChest = 0;
                                    isChestOpening = false;
                                    isGoldChestOpening = false;
                                }
                            }
                            else
                            {
                                if (keyState.IsKeyUp(Keys.E))
                                {
                                    timerChest = 0;
                                    isChestOpening = false;
                                    isGoldChestOpening = false;
                                }
                            }
                        }
                    }
                }

                if (drop != null)
                {
                    foreach (Sprite s in drop)
                    {
                        s.Update(gameTime, game);
                        foreach (Sprite ss in map)
                        {
                            if (s.IsTouchingTop(ss))
                                s.SetGravity(false);
                        }

                        if (player.destRectangle.Intersects(s.Rectangle))
                        {
                            timerPickUp += gameTime.ElapsedGameTime.Milliseconds;
                            if (timerPickUp >= 100)
                            {
                                timerPickUp = 0;
                                equipment.addToEq(s.getSpriteType());
                                drop.Remove(s);
                            }
                            break;
                        }
                    }
                }

                this.camera.Follow(player);
            }
            else
            {
                this.timeInPause += gameTime.ElapsedGameTime.Milliseconds;
                frameRateMin = 99999.0;
            }
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.End();
            spriteBatch.Begin(transformMatrix: camera.Transform);

            foreach (Sprite s in clouds)
            {
                if (s.destRectangle.X >= (player.destRectangle.X - Settings.ScreenWidth / 2 - 100)
                   && s.destRectangle.X <= (player.destRectangle.X + Settings.ScreenWidth / 2 + 100)
                   && s.destRectangle.Y <= (player.destRectangle.Y + Settings.ScreenHeight / 2 + 100)
                   && s.destRectangle.Y >= (player.destRectangle.Y - Settings.ScreenHeight / 2 - 100))
                {
                    s.Draw(spriteBatch);
                }
            }

            foreach (Sprite s in map)
            {
                if (s.destRectangle.X >= (player.destRectangle.X - Settings.ScreenWidth / 2 - 100)
                    && s.destRectangle.X <= (player.destRectangle.X + Settings.ScreenWidth / 2 + 100)
                    && s.destRectangle.Y <= (player.destRectangle.Y + Settings.ScreenHeight / 2 + 100)
                    && s.destRectangle.Y >= (player.destRectangle.Y - Settings.ScreenHeight / 2 - 100))
                {
                    s.Draw(spriteBatch);
                }
            }

            if (notifications != null)
            {
                foreach (Notification notif in notifications)
                {
                    spriteBatch.DrawString(spriteFont, notif.text, notif.GetVector2(), Color.Red, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);
                }
            }

            foreach (Sprite s in map)
            {
                if (s.getSpriteType() == SpriteType.CHEST || s.getSpriteType() == SpriteType.GOLDCHEST || s.getSpriteType() == SpriteType.DOOR)
                {
                    if (Math.Sqrt(Math.Pow(player.destRectangle.X - s.destRectangle.X, 2) + Math.Pow(player.destRectangle.Y - s.destRectangle.Y, 2)) < 80)
                        spriteBatch.DrawString(spriteFont, Text.press_e, new Vector2(s.destRectangle.X, s.destRectangle.Y - 10 * scale.GetScale()), Color.White);
                }
            }

            this.weapons[player.weaponID].Draw(spriteBatch);
            foreach (Weapon w in enemyWeapons)
                w.Draw(spriteBatch);
            this.player.Draw(spriteBatch);
            foreach (Enemy enemy in enemies)
                enemy.Draw(spriteBatch);

            if (drop != null)
            {
                foreach (Sprite s in drop)
                {
                    if (s.destRectangle.X >= (player.destRectangle.X - Settings.ScreenWidth / 2 - 100)
                   && s.destRectangle.X <= (player.destRectangle.X + Settings.ScreenWidth / 2 + 100)
                   && s.destRectangle.Y <= (player.destRectangle.Y + Settings.ScreenHeight / 2 + 100)
                   && s.destRectangle.Y >= (player.destRectangle.Y - Settings.ScreenHeight / 2 - 100))
                        s.Draw(spriteBatch);
                }
            }

            if (isPause)
                spriteBatch.Draw(Resources.Images["background"], new Rectangle(player.destRectangle.X - Settings.ScreenWidth / 2 + player.destRectangle.Width / 2, player.destRectangle.Y - Settings.ScreenHeight / 2 + player.destRectangle.Height / 2, Settings.ScreenWidth, Settings.ScreenHeight), Color.Black * 0.75f);

            if (isGoldChestOpening)
                spriteBatch.DrawString(spriteFont, String.Format("{0:F0}", (int)((Math.Abs(timerChest - 6000)) / 1000)), new Vector2(player.destRectangle.X + player.destRectangle.Width/2, player.destRectangle.Y + (int)(scale.GetScale() * 50)), Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);

            if (isChestOpening)
                spriteBatch.DrawString(spriteFont, String.Format("{0:F0}", (int)((Math.Abs(timerChest - 4000)) / 1000)), new Vector2(player.destRectangle.X + player.destRectangle.Width / 2, player.destRectangle.Y + (int)(scale.GetScale() * 50)), Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);


            //FPS counter
            //spriteBatch.DrawString(spriteFont, "FPS min: "+ String.Format("{0:F0}", frameRateMin), new Vector2(player.destRectangle.X-Settings.ScreenWidth/2, player.destRectangle.Y + Settings.ScreenHeight / 4 ), Color.ForestGreen);
            //spriteBatch.DrawString(spriteFont, "FPS max: " + String.Format("{0:F0}", frameRateMax), new Vector2(player.destRectangle.X - Settings.ScreenWidth / 2 , player.destRectangle.Y - Settings.ScreenHeight / 4), Color.ForestGreen);

            spriteBatch.End();
            spriteBatch.Begin();
        }
    }
}
