﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MonoGame
{
    class Inventory
    {
        private Scale scaleElement = new Scale();

        private Panel panel;

        private Icon icon_wood, icon_stone, icon_bone, icon_gold, icon_iron, icon_diamond, icon_hide, icon_meat;

        public Inventory(Equipment equipment)
        {
            // create a panel and position in center of screen
            float panelWidth = (float)(Settings.ScreenWidth * 0.35);
            float panelHeight = (float)(Settings.ScreenHeight * 0.3);

            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;

            panel = new Panel(new Vector2((float)panelWidth / scaleValue, (float)panelHeight / scaleValue), PanelSkin.Default); //Anchor.Center
            UserInterface.Active.AddEntity(panel);

            panel.AddChild(new Header(Text.inventory));

            icon_wood = new Icon(IconType.wood, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), ""+equipment.wood);
            icon_wood.ToolTipText = "Ilość: " + equipment.wood;
            icon_stone = new Icon(IconType.stoneOre, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.stone);
            icon_stone.ToolTipText = "Ilość: " + equipment.stone;
            icon_diamond = new Icon(IconType.diamondOre, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.diamond);
            icon_diamond.ToolTipText = "Ilość: " + equipment.diamond;
            icon_gold = new Icon(IconType.goldOre, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.gold);
            icon_gold.ToolTipText = "Ilość: " + equipment.gold;
            icon_iron = new Icon(IconType.ironOre, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.iron);
            icon_iron.ToolTipText = "Ilość: " + equipment.iron;
            icon_bone = new Icon(IconType.Bone, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.bone);
            icon_bone.ToolTipText = "Ilość: " + equipment.bone;
            icon_hide = new Icon(IconType.hide, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.hide);
            icon_hide.ToolTipText = "Ilość: " + equipment.hide;
            icon_meat = new Icon(IconType.meat, Anchor.AutoInline, scaleElement.GetScale(), false, new Vector2(10, 0), "" + equipment.meat);
            icon_meat.ToolTipText = "Ilość: " + equipment.meat;

            if (equipment.wood > 0)
                panel.AddChild(icon_wood);
            if (equipment.stone > 0)
                panel.AddChild(icon_stone);
            if (equipment.iron > 0)
                panel.AddChild(icon_iron);
            if (equipment.gold > 0)
                panel.AddChild(icon_gold);
            if (equipment.diamond > 0)
                panel.AddChild(icon_diamond);
            if (equipment.hide > 0)
                panel.AddChild(icon_hide);
            if (equipment.bone > 0)
                panel.AddChild(icon_bone);
            if (equipment.meat > 0)
                panel.AddChild(icon_meat);

            int totalItems = equipment.TotalItems();

            if (totalItems == 0)
            {
                panel.AddChild(new Paragraph(Text.inventory_empty, Anchor.Center));
            }

        }

        public void SetPanelVisible(bool isVisible)
        {
            panel.Visible = isVisible;
        }

    }
}