﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Map
    {
        //FIELDS
        private List<Sprite> mapList = new List<Sprite>();
        private List<Sprite> clouds = new List<Sprite>();
        private string[] lines;
        private int width;
        private int length;
        private Random random;
        private Scale scale = new Scale();
        private int textureSize = 70;
        private int counterWalkableElement = 0;
        private int timer;

        //CONSTRUCTOR
        public Map()
        {
            this.lines = System.IO.File.ReadAllLines(@"Content/Maps/map.txt");
            this.width = lines.Length;
            this.length = lines[0].Length;
            this.random = new Random();
            createMapList();
        }

        //METHODS
        private void createMapList()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (lines[i][j] == '1')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["dirt"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), false, 99999, SpriteType.OTHER));
                    }
                    else if (lines[i][j] == '2')
                    {
                        counterWalkableElement++;
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["blank"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    }
                    else if (lines[i][j] == '*')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["blank"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.SPAWN));
                    }
                    else if (lines[i][j] == '3')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["tree"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 10, SpriteType.WOOD));
                    }
                    else if (lines[i][j] == '4')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["stone"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 10, SpriteType.STONE));
                    }
                    else if (lines[i][j] == '5')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["iron"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 12, SpriteType.IRON));
                    }
                    else if (lines[i][j] == '6')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["chest"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.CHEST));
                    }
                    else if (lines[i][j] == '7')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["chestgold"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.GOLDCHEST));
                    }
                    else if (lines[i][j] == '8')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["diamond"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 20, SpriteType.DIAMOND));
                    }
                    else if (lines[i][j] == '9')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["gold"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 16, SpriteType.GOLD));
                    }
                    else if (lines[i][j] == '0')
                    {
                        AddCloud(i, j);
                        mapList.Add(new Sprite(Resources.Images["door"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.DOOR));
                    }
                    else if (lines[i][j] == ',')
                    {
                        continue;
                    }
                }
            }
        }

        public int GetCountWalkableElement()
        {
            return counterWalkableElement;
        }

        public List<Sprite> getList()
        {
            return mapList;
        }

        public List<Sprite> getClouds()
        {
            return clouds;
        }

        private void AddCloud(int i, int j)
        {
            int x = random.Next(0, 5);
            switch (x)
            {
                case 0:
                    clouds.Add(new Sprite(Resources.Images["air"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    break;
                case 1:
                    clouds.Add(new Sprite(Resources.Images["air1"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    break;
                case 2:
                    clouds.Add(new Sprite(Resources.Images["air2"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    break;
                case 3:
                    clouds.Add(new Sprite(Resources.Images["air3"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    break;
                case 4:
                    clouds.Add(new Sprite(Resources.Images["air"], j * (int)(textureSize * scale.GetScale()), i * (int)(textureSize * scale.GetScale()), true, 99999, SpriteType.AIR));
                    break;
            }
        }

        public virtual void Update(GameTime gameTime)
        {

            timer += gameTime.ElapsedGameTime.Milliseconds;
            if (timer >= 50)
            {
                timer = 0;
                foreach (Sprite cloud in clouds)
                {
                    cloud.destRectangle.X--;
                    if (cloud.destRectangle.X < 0)
                        cloud.destRectangle.X = (length - 1) * ((int)(textureSize * scale.GetScale()));
                }
            }
        }
    }
}
