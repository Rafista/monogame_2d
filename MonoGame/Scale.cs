﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{

    class Scale
    {
        private SpriteFont fontSprite;
        private float originalSizeWidth = (float)Settings.StartedScreenWidth;
        private float originalSizeHeight = (float)Settings.StartedScreenHeight;

        private ArrayList listAvailableResolution = new ArrayList();
        private string[] resolutions = new string[]{
            "800x600",
            "1024x768",
            "1280x720",
            "1366x768",
            "1280x800",
            "1440x900",
            "1280x1024",
            "1400x1050",
            "1024x600",
            "1680x1050",
            "1600x1200",
            "1600x900",
           "1920x1080"
        };

        public float GetScaleX()
        {
            return (float)(Settings.ScreenWidth / originalSizeWidth);
        }

        public float GetScaleY()
        {
            return (float)(Settings.ScreenHeight / originalSizeHeight);
        }

        public float GetScale()
        {
            float value = (float)(Settings.ScreenHeight / originalSizeHeight) + (float)(Settings.ScreenWidth / originalSizeWidth);
            float returnedValue = (float)(value / 2.0);
            return returnedValue;
        }

        public int TextWidth(string text, int font)
        {
            this.fontSprite = GeonBit.UI.Resources.Fonts[font];
            return (int)(fontSprite.MeasureString(text).X + Settings.ScreenWidth / 10);
        }

        public int TextHeight(string text, int font)
        {
            this.fontSprite = GeonBit.UI.Resources.Fonts[font];
            return (int)(fontSprite.MeasureString(text).Y + Settings.ScreenHeight / 40);
        }

        public ArrayList GetAvailableResolution()
        {
            for(int i = 0; i < resolutions.Length; i++)
            {
                if(Int32.Parse(resolutions[i].Split('x')[0])<=Settings.StartedScreenWidth && Int32.Parse(resolutions[i].Split('x')[1]) <= Settings.StartedScreenHeight)
                {
                    listAvailableResolution.Add(resolutions[i]);
                }
            }

            return listAvailableResolution;
        }
    }
}
