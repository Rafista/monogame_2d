﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class PlaySong
    {
        private Song song;
    
        public PlaySong(string songName, float volumeValue, bool repeat)
        {
            this.song = Resources.Songs[songName];
            MediaPlayer.IsRepeating = repeat;
            MediaPlayer.Volume = volumeValue;
        }

        public void Play()
        {
            MediaPlayer.Play(song);
        }

        public void Stop()
        {
            MediaPlayer.Stop();
        }

        public void ChangeVolumeSong(float volumeValue)
        {
            MediaPlayer.Volume = volumeValue;

        }
    }
}
