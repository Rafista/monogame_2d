﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    class Notification : MenuBase
    {
        public string text;
        public int x, y;
        public float timer;
        private float timerDirection = 0;
        private int direction = 1;


        public Notification(string text, int x, int y, float timer) : base()
        {
            this.text = text;
            this.x = x;
            this.y = y;
            this.timer = timer;
        }

        public Vector2 GetVector2()
        {
            return new Vector2(x, y);
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            this.timer -= gameTime.ElapsedGameTime.Milliseconds;
            this.timerDirection += gameTime.ElapsedGameTime.Milliseconds;

            if (timerDirection > 150)
            {
                timerDirection = 0;
                direction *= -1;
            }

            this.x += (int)(direction * 1);
            this.y -= 1;
        }


    }
}
