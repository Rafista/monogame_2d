﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Equipment
    {
        public enum SpriteType
        {
            OTHER,
            WOOD,
            STONE,
            IRON,
            GOLD,
            DIAMOND,
            BONE,     
            MEAT,
            HIDE,
            CHEST,
            GOLDCHEST
        }

        public int wood;
        public int stone;
        public int iron;
        public int gold;
        public int diamond;
        public int bone;
        public int hide;
        public int meat;

        public Equipment()
        {
            this.wood = 0;
            this.stone = 0;
            this.iron = 0;
            this.gold = 0;
            this.diamond = 0;
            this.bone = 0;
            this.hide = 0;
            this.meat = 0;
        }

        //METHODS

        internal void addToEq(MonoGame.SpriteType spriteType)
        {     
            switch (spriteType.ToString())
            {
                case "WOOD":
                    this.wood += 1;
                    break;
                case "STONE":
                    this.stone += 1;
                    break;
                case "IRON":
                    this.iron += 1;
                    break;
                case "GOLD":
                    this.gold += 1;
                    break;
                case "DIAMOND":
                    this.diamond += 1;
                    break;
                case "BONE":
                    this.bone += 1;
                    break;           
                case "MEAT":
                    this.meat += 1;               
                    break;
                case "HIDE":               
                    this.hide += 1;
                    break;
            }
        }

      public int TotalItems()
        {
            int total = wood+iron+gold+stone+diamond+bone+hide+meat;
            return total;
        }
      
    }
}
