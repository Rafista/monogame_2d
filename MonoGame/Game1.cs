﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using GeonBit.UI;
using GeonBit.UI.Entities;

namespace MonoGame
{
    public enum Scene
    {
        MENU,
        OPTIONS,
        CORE,
        ABOUT,
        LOADING
    }
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        MouseState oldMouse;

        float m_lastTime;

        MenuBase menu;
        ReadXml readXml;
        
        LoadSettings loadSettings = new LoadSettings();

        PlaySong playSong;

        public Game1()
        {        
            Settings.ScreenWidth = Settings.StartedScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Settings.ScreenHeight = Settings.StartedScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            loadSettings.Load(Settings.StartedScreenWidth, Settings.StartedScreenHeight);
            
            loadSettings.CheckExtistingOfFile();
            readXml = new ReadXml();
            readXml.SaveXmlContent();
            graphics = new GraphicsDeviceManager(this);
            this.IsMouseVisible = false;
            Content.RootDirectory = "Content";        
            SetVideoSettings();
        }

        public void SetVideoSettings()
        {
            graphics.PreferredBackBufferWidth = Settings.ScreenWidth;
            graphics.PreferredBackBufferHeight = Settings.ScreenHeight;
            graphics.IsFullScreen = Settings.Fullscreen;
            this.IsFixedTimeStep = false;
            graphics.ApplyChanges();
        }

        protected override void Initialize()
        {            
            UserInterface.Initialize(Content, BuiltinThemes.hd);
            UserInterface.Active.ShowCursor = false;
            base.Initialize();

            if (Settings.Fullscreen)
            {
                Settings.PixelRatio = 1920 / 1280;
                Settings.ScreenWidth = GraphicsDevice.DisplayMode.Width;
                Settings.ScreenHeight = GraphicsDevice.DisplayMode.Height;
                SetVideoSettings();
            }

            this.playSong = new PlaySong("bgmusic", (float)(Settings.masterVolume * Settings.musicVolume), true);
            playSong.Play();
            this.menu = new LoadingScreen("intro");
            m_lastTime = 0.0f;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Resources.LoadImages(this.Content);
            Resources.LoadSounds(this.Content);
            Resources.LoadSongs(this.Content);
           // Resources.LoadVideos(this.Content);
        }

        protected override void UnloadContent()
        {

        }

        public void ChangeMusicVolume()
        {
            playSong.ChangeVolumeSong((float)(Settings.masterVolume * Settings.musicVolume));
        }

        public void ChangeScene(Scene scene, string loadingScreenName)
        {
            switch (scene)
            {
                case Scene.MENU:
                    this.menu = new Menu();
                    break;
                case Scene.CORE:
                    this.menu = new Core();
                    break;
                case Scene.OPTIONS:
                    this.menu = new Options();
                    break;
                case Scene.ABOUT:
                    this.menu = new About();
                    break;
                case Scene.LOADING:
                    this.menu = new LoadingScreen(loadingScreenName);
                    break;
                default:
                    break;
            }
        }

        protected override void Update(GameTime gameTime)
        {
            //Globals.s_dt = nowyczas - m_lastTime;
            Globals.s_dt = gameTime.ElapsedGameTime.Milliseconds - m_lastTime;

            if (Globals.s_dt > (1.0f / 30.0f))
            {
                Globals.s_dt = 1.0f / 30.0f;
            }

            UserInterface.Active.Update(gameTime);
            this.menu.Update(gameTime, new Input(oldMouse, Mouse.GetState()), this);

            oldMouse = Mouse.GetState();

            base.Update(gameTime);

            //m_lastTime = tutaj pobrac aktualny czas
            m_lastTime = gameTime.ElapsedGameTime.Milliseconds;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            this.menu.Draw(spriteBatch);
            spriteBatch.End();
            UserInterface.Active.Draw(spriteBatch);         
            base.Draw(gameTime);
        }

    }
}
