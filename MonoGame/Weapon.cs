﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public enum WeaponType
    {
        SWORD,
        AXE,
        PICKAXE,
        SKELETONSWORD
    }

    public class Weapon : GameObject
    {
        //FIELDS
        private Player player;
        private Enemy enemy;
        private WeaponType type;
        private int timerAttackRight;
        private int timerAttackLeft;
        private float rotation;
        private static bool attackUpRight;
        private static bool attackDownRight;
        private static bool attackUpLeft;
        private static bool attackDownLeft;
        MouseState mouseState;
        private Scale scale = new Scale();

        private PlaySound weaponSound;

        //GETTERS & SETTERS
        public WeaponType getWeaponType()
        {
            return this.type;
        }

        public Enemy whomWeapon()
        {
            return this.enemy;
        }

        //CONSTRUCTOR
        public Weapon(Player player, WeaponType type) : base(player, type) //broń gracza
        {
            this.player = player;
            this.type = type;
            attackUpRight = false;
            attackDownRight = false;
            attackUpLeft = false;
            attackDownLeft = false;
        }

        public Weapon(Enemy enemy, WeaponType type) : base(enemy, type) //broń przeciwnika
        {
            this.player = null;
            this.enemy = enemy;
            this.type = type;
        }

        public static bool attack()
        {
            if (attackUpRight || attackDownRight || attackUpLeft || attackDownLeft)
                return true;
            else
                return false;
        }

        public void playSound()
        {
            switch (type)
            {
                case WeaponType.SWORD:
                    this.weaponSound = new PlaySound("sword", (float)(Settings.effectVolume * Settings.masterVolume));
                    break;
            }
        }

        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            base.Update(gameTime, input, game);
            if (player != null)
                this.destRectangle = this.player.Rectangle;
            else if (enemy != null)
                this.destRectangle = this.enemy.Rectangle;

            //POSITION
            this.destRectangle.X += (int)velocity.X;
            this.destRectangle.Y += (int)velocity.Y;

            //przerzucanie broni w zależności w którą stronę patrzy postać
            if (!attack())
                mouseState = Mouse.GetState();

            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick)
                {
                    if (state.ThumbSticks.Left.X < 0)
                        this.destRectangle.X -= ((int)(this.texture.Width*scale.GetScale()) / 2) +(int)(5*scale.GetScale());
                    else
                        this.destRectangle.X += ((int)(this.texture.Width * scale.GetScale()) / 2) + 5;
                }

                if (state.IsButtonDown(Buttons.X) && !attack())
                {
                    if (state.ThumbSticks.Left.X < 0)
                        attackUpLeft = true;
                    else
                        attackUpRight = true;
                }
                else if (input.IsLeftMousePressed() && !attack())
                {
                    if (mouseState.Position.X - ((int)(this.texture.Width * scale.GetScale()) / 2) > (Settings.ScreenWidth / 2 - (int)(20*scale.GetScale())))
                        attackUpRight = true;
                    else
                        attackUpLeft = true;
                }
            }
            else
            {
                if ((mouseState.Position.X - (texture.Width / 2) > (Settings.ScreenWidth / 2 - (int)(20 * scale.GetScale()))))
                    this.destRectangle.X += ((int)(this.texture.Width * scale.GetScale()) / 2) + (int)(5 * scale.GetScale());
                else
                    this.destRectangle.X -= ((int)(this.texture.Width * scale.GetScale()) / 2) + (int)(5 * scale.GetScale());

                if (input.IsLeftMousePressed() && !attack())
                {
                    if (mouseState.Position.X - ((int)(this.texture.Width * scale.GetScale()) / 2) > (Settings.ScreenWidth / 2 - (int)(20 * scale.GetScale())))
                        attackUpRight = true;
                    else
                        attackUpLeft = true;
                }
            }


            //right
            if (attackUpRight)
            {
                float i = 1;
                this.velocity.X -= 2f * i;
                this.rotation -= 0.08f * i;
                this.timerAttackRight += gameTime.ElapsedGameTime.Milliseconds;
                if (timerAttackRight >= 250)
                {
                    timerAttackRight = 0;
                    attackUpRight = false;
                    attackDownRight = true;
                }
            }

            if (attackDownRight)
            {
                playSound();
                float i = 1;
                this.velocity.X += 4f * i;
                this.rotation += 0.16f * i;
                this.timerAttackRight += gameTime.ElapsedGameTime.Milliseconds;
                if (timerAttackRight >= 125)
                {
                    timerAttackRight = 0;
                    attackDownRight = false;
                }
            }

            //left
            if (attackUpLeft)
            {
                float i = 1;
                this.velocity.Y -= 3f * i;
                this.velocity.X += 4f * i;
                this.rotation += 0.08f * i;
                this.timerAttackLeft += gameTime.ElapsedGameTime.Milliseconds;
                if (timerAttackLeft >= 250)
                {
                    timerAttackLeft = 0;
                    attackUpLeft = false;
                    attackDownLeft = true;
                }
            }

            if (attackDownLeft)
            {
                playSound();
                float i = 1;
                this.velocity.Y += 6f * i;
                this.velocity.X -= 8f * i;
                this.rotation -= 0.16f * i;
                this.timerAttackLeft += gameTime.ElapsedGameTime.Milliseconds;
                if (timerAttackLeft >= 125)
                {
                    timerAttackLeft = 0;
                    attackDownLeft = false;
                }
            }

            if (!attackUpRight && !attackDownRight && !attackUpLeft && !attackDownLeft)
            {
                this.velocity.Y = 0f;
                this.velocity.X = 0f;
                this.rotation = 0f;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!attack())
                mouseState = Mouse.GetState();

            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick)
                {
                    if (state.ThumbSticks.Left.X < 0)
                    {
                        //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, this.rotation, Vector2.Zero, scale.GetScale(), SpriteEffects.FlipHorizontally, 0f);

                    }
                    else
                    {
                        // spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.None, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, this.rotation, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);

                    }
                }
            }
            else
            {
                if (player != null)
                {
                    if (mouseState.Position.X - (this.player.Rectangle.Width / 2) > (Settings.ScreenWidth / 2 - 20))
                    {
                        //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.None, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, this.rotation, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);
                    }
                    else
                    {
                        // spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, this.rotation, Vector2.Zero, scale.GetScale(), SpriteEffects.FlipHorizontally, 0f);
                    }
                }
                else
                {
                    if (mouseState.Position.X - (this.enemy.Rectangle.Width / 2) > (Settings.ScreenWidth / 2 - 20))
                    {
                        spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.None, 0f);
                    }
                    else
                    {
                        spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, this.rotation, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
                    }
                }
            }
        }
    }
}
