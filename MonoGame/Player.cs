﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Player : GameObject
    {
        //FIELDS
        public float speed;
        private int timerJump;
        private int timerInventory;
        private int timerChangeWeapon;
        public float jump;
        private float jumpY;
        private bool jumped;
        private bool gravity;
        public bool isInventoryOpen;
        public int weaponID;

        MouseState mouseState;
        Inventory inventory;
        Core core;
        private Scale scale = new Scale();

        private PlaySound jumpSound;
        private PlaySound switchSound;

        //CONSTRUCTOR
        public Player(int x, int y, Core core) : base(x, y)
        {
            this.speed = 5f;
            this.jump = 30f;
            this.jumpY = jump;
            this.jumped = false;
            this.gravity = true;
            this.isInventoryOpen = false;
            this.core = core;
        }

        //METHODS
        public void SetGravity(bool gravity)
        {
            this.gravity = gravity;
        }

        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }

        public void MovePosition(int x, int y)
        {
            this.destRectangle.Y += y;
            this.destRectangle.X += x;
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            KeyboardState keyState = Keyboard.GetState();
            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected || !Settings.gamepad)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick || !Settings.gamepad)
                {

                    //GRAVITY
                    if (gravity)
                    {
                        this.destRectangle.Y += 3;
                    }


                    if (!isInventoryOpen)
                    {
                        //POSITION

                        this.destRectangle.Y += (int)velocity.Y;
                        this.destRectangle.X += (int)velocity.X;

                        //CHANGE WEAPON
                        if (keyState.IsKeyDown(Keys.Q) || state.IsButtonDown(Buttons.LeftShoulder))
                        {
                            this.switchSound = new PlaySound("switch", (float)(Settings.effectVolume * Settings.masterVolume));
                            this.timerChangeWeapon += gameTime.ElapsedGameTime.Milliseconds;
                            if (timerChangeWeapon >= 100)
                            {
                                timerChangeWeapon = 0;
                                if (weaponID < 2)
                                    weaponID++;
                                else
                                    weaponID = 0;

                                core.ReloadHud(weaponID);
                            }
                        }

                        //MOVEMENT   
                        if (keyState.IsKeyDown(Keys.D) || state.ThumbSticks.Left.X > 0)
                        {
                            this.velocity.X = speed;
                            animatePlayer(gameTime);
                        }
                        else if (keyState.IsKeyDown(Keys.A) || state.ThumbSticks.Left.X < 0)
                        {
                            this.velocity.X = -speed;
                            animatePlayer(gameTime);
                        }
                        else
                        {
                            this.velocity.X = 0f;
                            animatePlayer(0);
                        }

                        //JUMP
                        if ((keyState.IsKeyDown(Keys.Space) || state.IsButtonDown(Buttons.B)) && !jumped)
                        {
                            this.velocity.Y = -8.5f;
                            this.jumped = true;
                            this.jumpSound = new PlaySound("jump", (float)(Settings.effectVolume * Settings.masterVolume));
                        }

                        if (jumped)
                        {

                            float i = 1;
                            this.velocity.Y += 0.15f * i;
                            this.timerJump += gameTime.ElapsedGameTime.Milliseconds;
                            if (timerJump >= 500)
                            {
                                timerJump = 0;
                                jumped = false;
                            }
                        }

                        if (!jumped)
                        {
                            this.velocity.Y = 0f;
                        }

                        //RESTART
                        if (keyState.IsKeyDown(Keys.R))
                        {
                            this.destRectangle.X = 5500;
                            this.destRectangle.Y = -100;
                        }
                    }

                    //INVENTORY SHOW
                    this.timerInventory += gameTime.ElapsedGameTime.Milliseconds;

                    if (timerInventory > 150)
                    {
                        if ((keyState.IsKeyDown(Keys.I) || state.IsButtonDown(Buttons.Y)) && !jumped)
                        {
                            if (!this.isInventoryOpen)
                            {
                                this.isInventoryOpen = true;
                                inventory = new Inventory(core.equipment);
                                timerInventory = 0;
                            }
                            else
                            {
                                this.isInventoryOpen = false;
                                inventory.SetPanelVisible(false);
                                timerInventory = 0;
                            }
                        }
                    }
                }
            }
            base.Update(gameTime, input, game);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!Weapon.attack())
                mouseState = Mouse.GetState();

            GamePadCapabilities c = GamePad.GetCapabilities(PlayerIndex.One);
            if (c.IsConnected)
            {
                GamePadState state = GamePad.GetState(PlayerIndex.One);
                if (c.HasLeftXThumbStick)
                {
                    if (state.ThumbSticks.Left.X < 0)
                    {
                        //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);
                    }
                    else
                    {
                        //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
                        spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.FlipHorizontally, 0f);
                    }
                }
            }
            else
            {
                if ((mouseState.Position.X - ((int)(texture.Width * scale.GetScale()) / 2) > (Settings.ScreenWidth / 2 - (int)(20 * scale.GetScale()))))
                {
                    //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
                    spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.FlipHorizontally, 0f);
                }
                else
                {
                    //spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
                    spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);
                }
            }
        }
    }
}
